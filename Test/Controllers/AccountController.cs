﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Test.Entities;
using Test.Models;
using Test.Repositorys;
using Account = Test.Entities.Account;

namespace Test.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AccountController : ControllerBase
	{
		private readonly IRepository<Account> _accountRepo;
		public AccountController(IRepository<Account> AccountRepo)
		{
			this._accountRepo = AccountRepo;
		}

		// GET api/account
		[HttpGet]
		public IActionResult GetAll()
		{
			var accounts = _accountRepo.All();
			List<ResponseAccount> response = new List<ResponseAccount>();
			response = accounts.Select(x => new ResponseAccount
			{
				accaccount_id = x.account_id,
				name = x.name,
				transactions = x.transactions.Select(s => new ResponseTransaction { desc= s.tran_type == "+" ? "ฝากเงิน" : "ถอนเงิน", amount =s.amount }).ToList()
			}).ToList();

			return Ok(response);
		}

		// GET api/account/140325
		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			var account = _accountRepo.Get(id);
			ResponseAccount response = new ResponseAccount();
			response.accaccount_id = account.account_id;
			response.name = account.name;
			response.transactions = account.transactions.Select(x => new ResponseTransaction { desc = x.tran_type == "+" ? "ฝากเงิน" : "ถอนเงิน", amount = x.amount }).ToList();
			return Ok(response);
		}

	}
}