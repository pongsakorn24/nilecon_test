﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Entities
{
	[Table("tbl_account")]
	public class Account
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int account_id { get; set; }
		public string name { get; set; }
		public ICollection<Transaction> transactions { get;set;}
	}
}
