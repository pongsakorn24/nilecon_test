﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Entities
{
	public class AccountContext : DbContext
	{
		public AccountContext(DbContextOptions options)
			: base(options)
		{
		}

		public DbSet<Account> Accounts { get; set; }
		public DbSet<Transaction> Transactions { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Transaction>()
			.HasOne<Account>(s => s.account)
			.WithMany(g => g.transactions)
			.HasForeignKey(s => s.account_id);
		}
	}
}
