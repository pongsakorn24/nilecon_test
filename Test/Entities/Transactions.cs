﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Entities
{
	[Table("tbl_transaction")]
	public class Transaction
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int tran_id { get; set; }
		public string tran_type { get; set; }
		public int amount { get; set; }
		public int account_id { get; set; }
		public Account account { get; set; }
	}
}
