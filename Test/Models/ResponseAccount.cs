﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
	public class ResponseAccount
	{
		public int accaccount_id { get; set; }
		public string name { get; set; }
		public List<ResponseTransaction> transactions { get; set; }
	}
}
