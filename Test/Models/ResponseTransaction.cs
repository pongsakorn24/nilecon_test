﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
	public class ResponseTransaction
	{
		public string desc { get; set; }
		public int amount { get; set; }
	}
}
