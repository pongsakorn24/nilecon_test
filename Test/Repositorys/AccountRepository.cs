﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Entities;

namespace Test.Repositorys
{
	public class AccountRepository : IRepository<Account>
	{
		private readonly AccountContext _context;
		public AccountRepository(AccountContext context)
		{
			_context = context;
		}

		public ICollection<Account> All()
		{
			return _context.Accounts.Select(x => new Account {
				name = x.name,
				account_id = x.account_id,
				transactions = _context.Transactions.Where(t => t.account_id == x.account_id).OrderBy(o => o.tran_id).ToList()
			}).ToList();
		}

		public Account Get(int id)
		{
			return _context.Accounts.Where(x => x.account_id == id).Select(x => new Account
			{
				name = x.name,
				account_id = x.account_id,
				transactions = _context.Transactions.Where(t => t.account_id == x.account_id).OrderBy(o => o.tran_id).ToList()
			}).FirstOrDefault();
		}
	}
}
