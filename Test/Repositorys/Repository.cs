﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Repositorys
{
	public interface IRepository<TEntity>
	{
		ICollection<TEntity> All();

		TEntity Get(int id);
	}
}
